import {
  disableButton, enableButton, makeHidden, makeUnhidden,
} from '../utils/utils';

export const delay = (t) => (new Promise((res) => {
  setTimeout(() => res(true), t);
}));

export function subscribe(input, url, button) {
  disableButton(button);
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: input.value,
    }),
  }).then((r) => r.json())
    .then(async (data) => {
      if (data.error) {
        throw new Error(data.error);
      }
      console.log(data);
      await delay(3000);
      makeHidden(input, button);
    })
    .catch((error) => {
      window.alert(error);
    })
    .finally(() => {
      enableButton(button);
    });
}

export function unsubscribe(url, button, input) {
  disableButton(button);
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((r) => {
    if (r.ok) {
      return r.json();
    }
    return Promise.reject(r);
  })
    .then(async (data) => {
      console.log(data);
      await delay(3000);
      makeUnhidden(input, button);
    })
    .catch((error) => {
      window.alert(error.statusText);
    })
    .finally(() => {
      enableButton(button);
    });
}

export function makeGetRequest(url) {
  return fetch(url)
    .then((r) => {
      if (r.ok) {
        return r.json();
      }
      return Promise.reject(r);
    })
    .catch((error) => {
      console.log((error.statusText));
    });
}

export function sendAnalytics(url, analytics) {
    fetch(url, {
        method: 'POST',
        body: []
    }).then((r) => {
        if (r.ok) {
            return r.json();
        }
        return Promise.reject(r);
    }).catch((error) => {
            console.log(error.statusText);
        });
}
