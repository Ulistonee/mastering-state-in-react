import './App.css';
import {CommunitySection} from "./components/CommunitySection";
import {SubscriptionSection} from "./components/SubscriptionSection";

function App() {
  return (
    <>
      <CommunitySection />
      <SubscriptionSection />
    </>
  )
}

export default App;
