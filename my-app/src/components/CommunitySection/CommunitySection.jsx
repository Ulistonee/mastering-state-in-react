import '../css/style.css'
import {useEffect, useState} from "react";

function makeGetRequest(url) {
  return fetch(url)
    .then((r) => {
      if (r.ok) {
        return r.json();
      }
      return Promise.reject(r);
    })
    .catch((error) => {
      console.log((error.statusText));
    });
}

export const CommunitySection = () => {
  const [people, setPeople] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isHidden, setIsHidden] = useState(false);

  useEffect( () => {
    setLoading(true);
    makeGetRequest('http://localhost:3000/community')
      .then((r) => {
        setPeople(r);
      })
      .catch(console.error)
      .finally(() => {
          setLoading(false);
        }
      )
  }, [])

  const handleHideButton = () => {
    if (isHidden === false) {
      setIsHidden(true)
    }
    else {
      setIsHidden(false);
    }
  }

  return (
    <section className="app-section">
      <h2 className="app-title">
        Big Community Of People Like You
        <button className="app--hide-button" onClick={handleHideButton}>
          {isHidden ? 'Show section' : "Hide section"}
        </button>
      </h2>
      <div className={isHidden ? 'hidden' : ''}>
        <h3 className="app-subtitle">
          We’re proud of our products, and we’re really excited <br /> when we get feedback from our users.
        </h3>
        <ul className="app-gallery">
          {
            loading ? <li>Loading...</li> : people.map((person) => {
              return (
                <li key={person.id}>
                  <img src={person.avatar} alt="avatar" crossOrigin="anonymous" />
                  <p className="app-gallery__name">
                    {person.firstName + ' ' + person.lastName}
                  </p>
                  <p>
                    {person.position}
                  </p>
                </li>
              )
            })
          }
        </ul>
      </div>
    </section>
  )
}
