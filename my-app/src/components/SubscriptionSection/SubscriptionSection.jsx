import { validate } from "../../utils/email-validator";
import { delay } from "../../api/query";
import {useState} from "react";


export const SubscriptionSection = () => {
  const [emailInput, setEmailInput] = useState("");
  const [loading, setLoading] = useState(false);
  const [subscribed, setSubscribed] = useState(false);

  const submit = (e) => {
    e.preventDefault();

    if (subscribed) {
      setLoading(true);
      fetch('http://localhost:3000/unsubscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((r) => {
        if (r.ok) {
          return r.json();
        }
        return Promise.reject(r);
      })
        .then(async (data) => {
          await delay(3000);
          setSubscribed(false);
          setEmailInput('');
        })
        .catch((error) => {
          window.alert(error.statusText);
        })
        .finally(() => {
          setLoading(false);
        });
    } else if (validate(emailInput)) {
      setLoading(true);
      fetch('http://localhost:3000/subscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: emailInput,
        }),
      }).then((r) => r.json())
        .then(async (data) => {
          if (data.error) {
            throw new Error(data.error);
          }
          await delay(3000);
          setSubscribed(true)
        })
        .catch((error) => {
          window.alert(error);
        })
        .finally(() => {
          setLoading(false);
        });
      }
    };

  return (
    <section className="app-section app-section--image-join-our-program">
      <h2 className="app-title">
        Join Our Program
      </h2>
      <h3 className="app-subtitle">
        Sed do eiusmod tempor incididunt <br />ut labore et dolore magna aliqua.
      </h3>
      <form className="app-section__form" onSubmit={submit}>
        {!subscribed && <input value={emailInput} type="email" placeholder="Email" name="input"
                className={"app-section__input"} disabled={loading} onChange={(e) => {
          setEmailInput(e.target.value);
        }}/>}
        <button className={"app-section__button app-section__button--join-our-program" + (loading ? ' button--disabled' : '')} disabled={loading}>
          {subscribed ? 'Unsubscribe' : 'Subscribe'}
        </button>
      </form>
    </section>
  )
}
