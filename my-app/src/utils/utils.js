import '../components/css/style.css';
import { sendAnalytics } from "../api/query.js";

export function makeHidden(input, button) {
  input.classList.add('hidden');
  button.textContent = 'Unsubscribe';
  localStorage.setItem('subscribed', 'yes');
}

export function makeUnhidden(input, button) {
  input.classList.remove('hidden');
  button.textContent = 'Subscribe';
  localStorage.removeItem('subscribed');
  localStorage.removeItem('email');
  input.value = '';
}

export function disableButton(button) {
  button.style.opacity = 0.5;
  button.disabled = true;
}
export function enableButton(button) {
  button.style.opacity = 1;
  button.disabled = false;
}

export async function logPerformance(){
    const perfData = window.performance.timing;
    const fetchTime = perfData.responseEnd - perfData.fetchStart;;
    const pageLoadTime = perfData.loadEventEnd - perfData.navigationStart;
    performance.measureUserAgentSpecificMemory()
        .then((r) => {
            console.log({
                fetchTime: fetchTime,
                pageLoadTime: pageLoadTime,
                memoryUsage: r.bytes
            });
            sendAnalytics("http://localhost:3000/analytics/performance", {
            fetchTime: fetchTime,
            pageLoadTime: pageLoadTime,
            memoryUsage: r.bytes
        }) });

}



